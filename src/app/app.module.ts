import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ServerComponent } from './server/server.component';
import { SuccessAlertComponent } from './alerts/success/success-alert.component';
import { WarningAlertComponent } from './alerts/warning/warning-alert.component';
import { UserFormComponent } from './user-form/user-form.component';
import { DetailsLogComponent } from './details-log/details-log.component';
import { GameControlComponent } from './game/game-control/game-control.component';
import { OddComponent } from './game/odd/odd.component';
import { EvenComponent } from './game/even/even.component';
import { GameComponent } from './game/game.component';


@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    SuccessAlertComponent,
    WarningAlertComponent,
    UserFormComponent,
    DetailsLogComponent,
    GameControlComponent,
    OddComponent,
    EvenComponent,
    GameComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
