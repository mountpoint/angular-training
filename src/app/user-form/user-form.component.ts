import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserFormComponent implements OnInit {
  username: string;
  usernames = [];

  constructor() { }

  ngOnInit() {
  }

  addUser() {
    this.usernames.push(this.username);
    this.username = '';
  }

}
