import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GameComponent implements OnInit {
  oddNumbers: number[] = [];
  evenNumbers: number[] = [];

  constructor() { }

  ngOnInit() {
  }

  onGameStarted(index: number) {
    if (index % 2 === 0) {
      this.evenNumbers.push(index);
    } else {
      this.oddNumbers.push(index);
    }
  }
}
