import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GameControlComponent implements OnInit {
  @Output() intervalFired = new EventEmitter<number>();

  interval;
  index = 0;

  constructor() { }

  ngOnInit() {
  }

  onStartGame() {
    this.interval = setInterval(() => {
      this.intervalFired.emit(++this.index);
    }, 1000);
  }

  onPauseGame() {
    clearInterval(this.interval);
  }
}
