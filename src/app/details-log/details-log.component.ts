import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Log } from '../shared/models/log';

@Component({
  selector: 'app-details-log',
  templateUrl: './details-log.component.html',
  styleUrls: ['./details-log.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DetailsLogComponent implements OnInit {
  showParagraph: boolean = true;
  logs: Log[] = [];

  constructor() { }

  ngOnInit() {
  }

  toggleParagraph() {
    this.showParagraph = !this.showParagraph;

    this.addLog();
  }

  addLog() {
    let log = new Log('Click', `${(new Date).toLocaleDateString()} ${(new Date).toLocaleTimeString()}`);

    this.logs.push(log);
  }
}
